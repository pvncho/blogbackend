from django.urls import path

from proyectodjango.views import ProyectoDjangoIndex

app_name = 'proyectodjango'

urlpatterns = [
    path('', ProyectoDjangoIndex.as_view(), name='home')
]
